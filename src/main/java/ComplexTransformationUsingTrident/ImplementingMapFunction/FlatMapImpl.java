package ComplexTransformationUsingTrident.ImplementingMapFunction;

import org.apache.storm.trident.operation.FlatMapFunction;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;

import java.util.ArrayList;
import java.util.List;

public class FlatMapImpl implements FlatMapFunction {
    @Override
    public Iterable<Values> execute(TridentTuple tridentTuple) {
        String str = tridentTuple.getString(0);
        List<Values> ret = new ArrayList<Values>();
        String[] split = str.split(" ");
        for(int i = 0; i < split.length; i++) {
            ret.add(new Values(split[i]));
        }
        return ret;
    }
}
