package ComplexTransformationUsingTrident.ImplementingMapFunction;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.LocalDRPC;
import org.apache.storm.trident.TridentTopology;

public class Topology {
    public static void main (String[] args) {
        TridentTopology tridentTopology = new TridentTopology();
        LocalDRPC localDRPC = new LocalDRPC();

        tridentTopology.newDRPCStream("lowerCaseAndSplit", localDRPC).map(new MapImpl()).flatMap(new FlatMapImpl());

        Config config = new Config();
        config.setDebug(true);

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("mapFlatMapImpl", config, tridentTopology.build());

        String word[] = {"Hmmmmmmmmm", "I don't know", "WHAT will haPPen"};

        for(int i = 0; i < word.length; i++) {
            System.out.println("Result for " + word[i] + " is: " + localDRPC.execute( "lowerCaseAndSplit", word[i]));
        }

        cluster.shutdown();


    }
}
