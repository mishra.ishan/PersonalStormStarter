package ComplexTransformationUsingTrident.BasicTridentTopology;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.LocalDRPC;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.tuple.Fields;

public class SimpleTridentExample {
    public static void main (String[] args) throws Exception{
        LocalDRPC drpc = new LocalDRPC();
        TridentTopology tridentTopology = new TridentTopology();
        tridentTopology.newDRPCStream("simple", drpc)
                .each(new Fields("Args"), new SimpleFunction(), new Fields("tuple after proc."));

        Config config = new Config();
        config.setDebug(true);

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("trident", config, tridentTopology.build());

        String[] words = {"abc" , "def", "ijk", "lmn"};
        for(String word : words) {
            System.out.println("word --> " + drpc.execute("simple", word));
        }

        cluster.shutdown();
    }
}
