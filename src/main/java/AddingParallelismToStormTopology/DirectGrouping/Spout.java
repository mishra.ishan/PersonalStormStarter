package AddingParallelismToStormTopology.DirectGrouping;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.List;
import java.util.Map;

public class Spout extends BaseRichSpout {
    private SpoutOutputCollector collector;
    private List<Integer> boltIds;
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
        this.boltIds = topologyContext.getComponentTasks("bolt");
    }

    public void nextTuple() {
        for(int i = 0; i < 100; i++) {
            collector.emitDirect(boltIds.get(i%boltIds.size()) ,new Values(i, i/10));
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("number", "bucket"));
    }
}
