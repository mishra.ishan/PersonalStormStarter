package AddingParallelismToStormTopology.ShuffleGrouping;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class Topology {
    public static void main(String[] args) throws InterruptedException{
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("spout", new Spout());
        builder.setBolt("bolt", new Bolt(), 2).shuffleGrouping("spout");

        Config conf = new Config();
        conf.setDebug(true);
        conf.put("writeToDir", "./src/main/resources/");

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("topology", conf, builder.createTopology());

        Thread.sleep(7000);
        cluster.shutdown();
    }
}
