package AddingParallelismToStormTopology.ShuffleGrouping;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class Bolt extends BaseBasicBolt {

    private File file;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        String fileName = String.valueOf(stormConf.get("writeToDir")) + context.getThisTaskId() + context.getThisComponentId() + ".txt";
        file = new File(fileName);
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        int no = tuple.getInteger(0);
        int bucket = tuple.getIntegerByField("bucket");
        String text = no + ":" + bucket;
        try {
            bufferedWriter.write(text);
            bufferedWriter.flush();
            bufferedWriter.newLine();
            basicOutputCollector.emit(new Values(text));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("no:bucket"));
    }

    @Override
    public void cleanup() {
        try {
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.cleanup();
    }
}
