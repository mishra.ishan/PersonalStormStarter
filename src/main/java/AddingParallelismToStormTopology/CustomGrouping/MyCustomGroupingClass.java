package AddingParallelismToStormTopology.CustomGrouping;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;

import java.util.ArrayList;
import java.util.List;

public class MyCustomGroupingClass implements CustomStreamGrouping {

    private List<Integer> boltIds;

    public void prepare(WorkerTopologyContext workerTopologyContext, GlobalStreamId globalStreamId, List<Integer> list) {
        this.boltIds = list;
    }

    public List<Integer> chooseTasks(int i, List<Object> list) {
        if (!list.isEmpty()) {
            List<Integer> ret = new ArrayList<Integer>();
            try {
                Integer boltIdToWhichBeSent = Integer.parseInt(String.valueOf(list.get(1))) % boltIds.size();
                ret.add(boltIds.get(boltIdToWhichBeSent));
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            return ret;
        }
        return null;
    }
}
