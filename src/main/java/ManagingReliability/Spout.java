package ManagingReliability;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Spout extends BaseRichSpout {

    private SpoutOutputCollector collector;
    private static final int MAX_FAILURE_ALLOWED = 3;
    private Map<Integer, Integer> failureCount;
    private List<Integer> integerToBeSent;

    @Override
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
        this.failureCount = new HashMap<Integer, Integer>();
        this.integerToBeSent = new ArrayList<Integer>();
        for(int i = 0; i < 100; i++) {
            integerToBeSent.add(i);
        }
    }

    @Override
    public void nextTuple() {
        if(!integerToBeSent.isEmpty()) {
            for(int a : integerToBeSent) {
                int bucket = a / 10;
                collector.emit(new Values(a, bucket), a);
            }
            integerToBeSent.clear();
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("integer", "bucket"));
    }

    @Override
    public void ack(Object msgId) {
        System.out.println("Successfully read: " + msgId);
    }

    @Override
    public void fail(Object msgId) {
        Integer failedInteger = (Integer) msgId;
        int failCountTillNow = 1;
        if(failureCount.containsKey(failedInteger)) {
            failCountTillNow = failureCount.get(failedInteger);
            failureCount.put(failedInteger, failCountTillNow + 1);
        }
        else {
            failureCount.put(failedInteger, 1);
        }
        if(failCountTillNow < MAX_FAILURE_ALLOWED) {
            integerToBeSent.add(failedInteger);
            System.out.println("Retrying integer: " + failedInteger);
        }
        else {
            System.out.println("Failed Integer: " + failedInteger);
        }
    }
}
