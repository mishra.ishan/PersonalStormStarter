package ManagingReliability;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.Map;
import java.util.Random;

public class Bolt extends BaseRichBolt {

    private static final int FAIL_PERCENTAGE = 80;
    private OutputCollector collector;
    private Random random;

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        this.collector = outputCollector;
        random = new Random();
    }

    @Override
    public void execute(Tuple tuple) {
        int a = random.nextInt(100);
        if(a < FAIL_PERCENTAGE) {
            collector.emit(tuple, new Values(a, a/10));
            collector.ack(tuple);
        }
        else {
            collector.fail(tuple);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("integer", "bucket"));
    }
}
