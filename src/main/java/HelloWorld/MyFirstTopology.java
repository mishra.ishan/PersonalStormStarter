package HelloWorld;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class MyFirstTopology {
    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder topologyBuilder = new TopologyBuilder();
        topologyBuilder.setSpout("my-first-spout", new MyFirstSpout());
        topologyBuilder.setBolt("my-first-bolt", new MyFirstBolt()).shuffleGrouping("my-first-spout");

        Config conf = new Config();
        conf.setDebug(true);

        LocalCluster localCluster = new LocalCluster();
        try {
            localCluster.submitTopology("my-first-topology", conf, topologyBuilder.createTopology());
            Thread.sleep(2000);
        }
        finally {
            localCluster.shutdown();
        }
    }
}
