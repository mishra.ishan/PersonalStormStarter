package RemoteDRPCClient.LocalMode;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.LocalDRPC;
import org.apache.storm.drpc.LinearDRPCTopologyBuilder;

public class Topology {
    public static void main(String[] args) throws InterruptedException {
        LinearDRPCTopologyBuilder builder = new LinearDRPCTopologyBuilder("plusTen");
        builder.addBolt(new Bolt(), 3);

        Config config = new Config();

        LocalDRPC localDRPC = new LocalDRPC();
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("plusTenTopology", config, builder.createLocalTopology(localDRPC));

        for(int a : new int[]{45, 23, 26}) {
            System.out.println("Result for number: " + a + " is " + localDRPC.execute("plusTen", String.valueOf(a)));
        }

        Thread.sleep(15000);
        cluster.shutdown();
    }
}
