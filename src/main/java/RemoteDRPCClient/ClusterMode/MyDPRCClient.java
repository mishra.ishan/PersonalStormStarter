package RemoteDRPCClient.ClusterMode;

import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.DRPCExecutionException;
import org.apache.storm.thrift.TException;
import org.apache.storm.thrift.transport.TTransportException;
import org.apache.storm.utils.DRPCClient;
import org.apache.storm.utils.Utils;

import java.util.Map;

public class MyDPRCClient {
    public static void main(String[] args) {
        Map config = Utils.readStormConfig();
        DRPCClient drpcClient;
        try {
            drpcClient = new DRPCClient(config, "localhost", 3372);
            for(int a : new int[]{12,23,56}) {
                System.out.println("Result for " + a + " is " + drpcClient.execute("plusTen", String.valueOf(a)));
            }
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (DRPCExecutionException e) {
            e.printStackTrace();
        } catch (AuthorizationException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        }
    }
}
