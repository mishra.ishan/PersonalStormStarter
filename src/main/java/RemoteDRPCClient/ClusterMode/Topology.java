package RemoteDRPCClient.ClusterMode;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.drpc.LinearDRPCTopologyBuilder;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;

import java.util.ArrayList;
import java.util.List;

public class Topology {
    public static void main(String[] args) {
        LinearDRPCTopologyBuilder builder = new LinearDRPCTopologyBuilder("plusTen");
        builder.addBolt(new Bolt());

        Config config = new Config();
        List<String> drpcServers = new ArrayList<String>();
        drpcServers.add("localhost");
        config.put(Config.DRPC_SERVERS, drpcServers);
        config.put(Config.DRPC_PORT, 3372);

        try {
            StormSubmitter.submitTopology("plusTen", config, builder.createRemoteTopology());
        } catch (AlreadyAliveException e) {
            e.printStackTrace();
        } catch (InvalidTopologyException e) {
            e.printStackTrace();
        } catch (AuthorizationException e) {
            e.printStackTrace();
        }
    }
}
