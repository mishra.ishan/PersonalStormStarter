package FileReader;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class FileReaderTopology {
    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("file-reader-spout", new FileReaderSpout());
        builder.setBolt("file-reader-bolt", new FileReaderBolt()).shuffleGrouping("file-reader-spout");

        Config conf = new Config();
        conf.setDebug(true);
        conf.put("file_read", "src/main/resources/file.txt");

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("file-reader-cluster", conf, builder.createTopology());
        Thread.sleep(5000);
        cluster.shutdown();
    }
}
