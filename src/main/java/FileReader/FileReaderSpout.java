package FileReader;

import org.apache.storm.shade.org.apache.commons.lang.StringUtils;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class FileReaderSpout extends BaseRichSpout{
    private FileReader fileReader;
    private SpoutOutputCollector collector;
    private BufferedReader reader;
    private String str;
    private boolean isCompleted;
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
        try {
            this.fileReader = new FileReader(String.valueOf(map.get("file_read")));
        }
        catch (FileNotFoundException ex) {

        }
        this.reader = new BufferedReader(fileReader);
        this.isCompleted = false;
    }

    public void nextTuple() {
        if(!isCompleted) {
            try {
                str = reader.readLine();
                System.out.println("String: " + str);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(StringUtils.isEmpty(str)) {
                System.out.println("String is empty");
                isCompleted = true;
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("String value later is: " + str);
                try {
                    collector.emit(new Values(str));
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("words"));
    }
}
