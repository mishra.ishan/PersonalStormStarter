package AccessingDataFromTuplesAndWritingToAFile;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.io.*;
import java.util.Map;

public class EmitterBolt extends BaseBasicBolt {

    private File file;
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        String fileName = context.getThisTaskId() + context.getThisComponentId() + ".txt";
        boolean fileExist = false;
        try {
            this.file = new File(String.valueOf(stormConf.get("dirToWrite")) + fileName);
            if(!file.exists()) {
                file.createNewFile();
            }
            this.fileWriter = new FileWriter(file);
            this.bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        String fname = tuple.getStringByField("fName");
        String lname = tuple.getString(2);

        String strToWrite = fname + ":" + lname;
        try {
            bufferedWriter.write(strToWrite);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        basicOutputCollector.emit(new Values(fname, lname));
    }

    @Override
    public void cleanup() {
        try {
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("fname", "lname"));
    }
}
