package AccessingDataFromTuplesAndWritingToAFile;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class TopologyMain {
    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("readerSpout", new ReaderSpout());
        builder.setBolt("emitterBolt", new EmitterBolt()).shuffleGrouping("readerSpout");

        Config conf = new Config();
        conf.setDebug(true);
        conf.put("file_path", "./src/main/resources/fieldReader.txt");
        conf.put("dirToWrite", "./src/main/resources/");

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("topologyMain", conf, builder.createTopology());
        Thread.sleep(10000);
        cluster.shutdown();
    }
}
