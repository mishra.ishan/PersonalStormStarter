package AccessingDataFromTuplesAndWritingToAFile;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;

import java.io.*;
import java.util.Map;

public class ReaderSpout extends BaseRichSpout {
    private SpoutOutputCollector collector;
    private boolean isCompleted;
    private FileInputStream fis;
    private BufferedReader bufferedReader;
    private String str;

    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
        try {
            fis = new FileInputStream(new File(String.valueOf(map.get("file_path"))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bufferedReader = new BufferedReader(new InputStreamReader(fis));
        isCompleted = false;
    }

    public void nextTuple() {
        if(!isCompleted) {
            try {
                str = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(str != null) {
                collector.emit(new Values(str.split(",")));
            }
            else {
                try {
                    bufferedReader.close();
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                isCompleted = true;
            }
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("id", "fName", "lName", "gender"));
    }
}
