package WordCountTopology;

import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CountBolt extends BaseBasicBolt {

    private PrintWriter printWriter;
    private Map<String, Integer> counter;
    private String fileName;

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        fileName = String.valueOf(stormConf.get("dirToWrite")) + context.getThisComponentId() + "-" + context.getThisTaskId() + ".txt";
        try {
            printWriter = new PrintWriter(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        counter = new HashMap<String, Integer>();
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        String word = tuple.getString(0);
        if(counter.containsKey(word)) {
            counter.put(word, counter.get(word) + 1);
        }
        else {
            counter.put(word, 1);
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }

    @Override
    public void cleanup() {
        String header = "WORD\t:\tCOUNT";
        printWriter.println(header);
        for(Map.Entry<String, Integer> pair : counter.entrySet()) {
            String word = pair.getKey();
            Integer count = pair.getValue();
            String str = word + "\t:\t" + count;
            printWriter.println(str);
        }
        printWriter.close();
    }
}
