package WordCountTopology;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class WordCountTopology {
    public static void main(String[] args) {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("sentenceReader", new SentenceReaderSpout());
        builder.setBolt("splitBolt", new SplitBolt()).shuffleGrouping("sentenceReader");
        builder.setBolt("wordCountBolt", new CountBolt(), 2).fieldsGrouping("splitBolt", new Fields("word"));

        Config config = new Config();
        config.setDebug(true);
        config.put("fileToRead", "./src/main/resources/sentenceReader.txt");
        config.put("dirToWrite", "./src/main/resources/");

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("wordCountTopology", config, builder.createTopology());
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cluster.shutdown();
    }
}
