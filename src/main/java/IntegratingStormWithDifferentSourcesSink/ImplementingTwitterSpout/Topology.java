package IntegratingStormWithDifferentSourcesSink.ImplementingTwitterSpout;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class Topology {
    public static void main (String[] args) {
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("spout", new TwitterSpout());
        builder.setBolt("bolt", new Bolt()).shuffleGrouping("spout");

        Config config = new Config();
        config.setDebug(true);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("topology", config, builder.createTopology());
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cluster.shutdown();
    }
}
