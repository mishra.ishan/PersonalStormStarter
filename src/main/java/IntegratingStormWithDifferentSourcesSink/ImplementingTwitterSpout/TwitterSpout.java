package IntegratingStormWithDifferentSourcesSink.ImplementingTwitterSpout;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class TwitterSpout extends BaseRichSpout {

    private SpoutOutputCollector collector;
    private Queue<Status> tweets;
    private TwitterStream twitterStream;

    @Override
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        this.collector = spoutOutputCollector;
        this.tweets = new LinkedBlockingQueue<Status>();
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setDebugEnabled(true).setOAuthConsumerKey("E7QF6xnfawOAL3W8vpspjEO1o");
        builder.setOAuthConsumerSecret("QWpALgpFubN6hLz9lKm123f82L71JA4oum4DUtr0i1uV0gaRTj");
        builder.setOAuthAccessToken("927084821532196865-ijKGZenyfQCFsl0yCLX4nn19QTJxKAu");
        builder.setOAuthAccessTokenSecret("P8S0HoN5ThOUidusoneOp7WesCfg1A65HsAnE7J013EyV");

        this.twitterStream = new TwitterStreamFactory(builder.build()).getInstance();
        final StatusListener statusListener= new StatusListener() {
            @Override
            public void onException(Exception e) {

            }

            @Override
            public void onStatus(Status status) {
                tweets.offer(status);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

            }

            @Override
            public void onTrackLimitationNotice(int i) {

            }

            @Override
            public void onScrubGeo(long l, long l1) {

            }

            @Override
            public void onStallWarning(StallWarning stallWarning) {

            }
        };

        twitterStream.addListener(statusListener);

        final FilterQuery query = new FilterQuery();
        query.track(new String[]{"chocolate"});
        twitterStream.filter(query);

    }

    @Override
    public void nextTuple() {
        Status status = tweets.poll();
        if(status == null) {
            Utils.sleep(50);
        }
        else {
            collector.emit(new Values(status));
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("tweet"));
    }
}
